<?php
class font extends colored {
    public $sizes;
    public function __set($property, $value){
        parent::__set($property , $value);
        $sizes = range(10,24);
        if($property == 'size'){

            if (in_array($value, $sizes)){
                $this->size = $value;
            }
            else{
                die("Change the size");
            }
        }
    }
 
  #commit 1
}
 class htmlpage {
    public $title = 'Title';
    public $body = 'Body';
    function __construct($title="", $body="") {
            $this->title=$title;
            $this->body=$body;
      }
      public function view(){
        echo "<html>
                <head>
                    <title>$this->title</title>
                </head>
                <body>
                 <p style = 'color:$this->color; 
                            font-size:$this->size'pt;>
                            $this->body </p>
                </body>
            </html>";
    }
}
?>